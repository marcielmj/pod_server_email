
package br.edu.ifpb.pod.mail;

import br.edu.ifpb.emailsharedpod.Email;
import java.io.UnsupportedEncodingException;
import java.util.Properties;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 *
 * @author marciel
 */
public class SendMail {
    
    private static final String MAIL = "ifpbpod@gmail.com";
    private static final String PASS = "rmi12345";
    
    private static Properties getProperties() {
        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");
        
        return props;
    }
    
    private static Authenticator getAuthenticator() {
        return new Authenticator() {  
            
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(MAIL, PASS);
            }
        };
    }
    
    private static Session getSession() {
        return Session.getInstance(getProperties(), getAuthenticator());
    }
    
    public static boolean send(Email mail) {
        
        boolean result = false;
        
        try {
            
            Message message = new MimeMessage(getSession());
            message.setFrom(new InternetAddress(MAIL, mail.getRemetente()));
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(mail.getDestinatarios()));
            message.setSubject(mail.getAssunto());
            message.setText(mail.getMensagem());

            Transport.send(message);

            result = true;

        } catch (MessagingException | UnsupportedEncodingException e) {
            System.out.println("Oops, ocorreu um erro ao enviar o email...");
        }
        
        return result;
    }
}