package br.edu.ifpb.pod.task;

import br.edu.ifpb.emailsharedpod.Email;
import br.edu.ifpb.pod.mail.SendMail;
import br.edu.ifpb.pod.persistence.Emails;
import br.edu.ifpb.pod.ping.PingGmail;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.TimerTask;

/**
 *
 * @author marciel
 */
public class Schedule extends TimerTask {

    @Override
    public void run() {

        Emails repo = new Emails();
        List<Email> mails = repo.getUnsentEmails();

        if (!mails.isEmpty()) {

            for (Email email : mails) {
                if (PingGmail.ping()) {
                    SendMail.send(email);
                    repo.updateStatusToSent(email.getId());
                }
            }

        }
    }
}
