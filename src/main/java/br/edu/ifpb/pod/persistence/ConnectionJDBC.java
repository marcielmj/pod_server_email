
package br.edu.ifpb.pod.persistence;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author marciel
 */
public class ConnectionJDBC {
    private static final String URL  = "jdbc:postgresql://localhost:5432/server_email_pod";
    private static final String USER = "postgres";
    private static final String PASS = "12345";
    
    public static Connection getConnection() {
        Connection connection = null;
        try {
            Class.forName("org.postgresql.Driver");
            connection = DriverManager.getConnection(URL, USER, PASS);
        } catch (SQLException | ClassNotFoundException ex) {
            ex.printStackTrace();
        }
        
        return connection;
    }
}
