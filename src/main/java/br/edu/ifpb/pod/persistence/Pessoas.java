package br.edu.ifpb.pod.persistence;

import br.edu.ifpb.emailsharedpod.Pessoa;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author marciel
 */
public class Pessoas {

    public boolean persist(Pessoa p) {

        String sql = "INSERT INTO pessoa (nome, email) VALUES (?,?)";

        try (Connection conn = ConnectionJDBC.getConnection(); PreparedStatement stmt = conn.prepareStatement(sql)) {

            stmt.setString(1, p.getNome());
            stmt.setString(2, p.getEmail());

            stmt.executeUpdate();

        } catch (SQLException ex) {
            System.out.println("Oops, ocorreu um erro ao executar o SQL...");
        }

        return false;
    }

    public boolean remove(int id) {

        boolean result = false;
        String sql = "DELETE FROM pessoa WHERE id = ?";

        try (Connection conn = ConnectionJDBC.getConnection(); PreparedStatement stmt = conn.prepareStatement(sql)) {
            stmt.setInt(1, id);
            stmt.executeUpdate();

        } catch (SQLException ex) {
            System.out.println("Oops, ocorreu um erro ao executar o SQL...");
        }

        return false;
    }

    public List<Pessoa> findAll() {

        String sql = "SELECT * FROM pessoa";
        List<Pessoa> pessoas = new ArrayList<>();

        try (Connection conn = ConnectionJDBC.getConnection(); PreparedStatement stmt = conn.prepareStatement(sql)) {

            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                Pessoa p = new Pessoa();
                p.setId(rs.getInt("id"));
                p.setNome(rs.getString("nome"));
                p.setEmail(rs.getString("email"));

                pessoas.add(p);
            }

        } catch (SQLException ex) {
            System.out.println("Oops, ocorreu um erro ao executar o SQL...");
        }

        return pessoas;
    }
}
