package br.edu.ifpb.pod.persistence;

import br.edu.ifpb.emailsharedpod.Email;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author marciel
 */
public class Emails {

    public boolean persist(Email email) {

        boolean result = false;
        String sql = "INSERT INTO EMAIL(remetente,destinatarios,assunto,ipServidor,status,mensagem) VALUES(?,?,?,?,?,?)";

        try (Connection conn = ConnectionJDBC.getConnection(); PreparedStatement stmt = conn.prepareStatement(sql)) {

            stmt.setString(1, email.getRemetente());
            stmt.setString(2, email.getDestinatarios());
            stmt.setString(3, email.getAssunto());
            stmt.setString(4, email.getIpServidor());
            stmt.setBoolean(5, email.isStatus());
            stmt.setString(6, email.getMensagem());
            stmt.executeUpdate();

            result = true;

        } catch (SQLException ex) {
            System.out.println("Oops, ocorreu um erro ao executar o SQL...");
        }

        return result;
    }

    public boolean updateStatusToSent(int id) {

        boolean result = false;
        String sql = "UPDATE EMAIL SET status=true WHERE id = ?";

        try (Connection conn = ConnectionJDBC.getConnection(); PreparedStatement stmt = conn.prepareStatement(sql)) {

            stmt.setInt(1, id);
            stmt.executeUpdate();

            result = true;

        } catch (SQLException ex) {
            System.out.println("Oops, ocorreu um erro ao executar o SQL...");
        }

        return result;
    }

    public List<Email> getUnsentEmails() {

        String sql = "SELECT id,remetente,destinatarios,assunto,status,mensagem,ipServidor FROM email WHERE status = false";

        List<Email> emails = new ArrayList<>();
        try (Connection conn = ConnectionJDBC.getConnection(); PreparedStatement stmt = conn.prepareStatement(sql)) {

            ResultSet rs = stmt.executeQuery();
            Email email = new Email();
            while (rs.next()) {
                email.setId(rs.getInt("id"));
                email.setMensagem(rs.getString("mensagem"));
                email.setRemetente(rs.getString("remetente"));
                email.setDestinatarios(rs.getString("destinatarios"));
                email.setAssunto(rs.getString("assunto"));
                email.setStatus(rs.getBoolean("status"));
                email.setIpServidor(rs.getString("ipServidor"));
                emails.add(email);
                email = new Email();
            }

        } catch (SQLException ex) {
            System.out.println("Oops, ocorreu um erro ao executar o SQL...");
        }

        return emails;

    }
}
