
package br.edu.ifpb.pod.rmi;

import br.edu.ifpb.emailsharedpod.Email;
import br.edu.ifpb.emailsharedpod.Fachada;
import br.edu.ifpb.emailsharedpod.Pessoa;
import br.edu.ifpb.pod.persistence.Emails;
import br.edu.ifpb.pod.persistence.Pessoas;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.List;

/**
 *
 * @author marciel
 */
public class EmailServer extends UnicastRemoteObject implements Fachada {

    public EmailServer() throws RemoteException{}

    @Override
    public String enviaEmail(Email email) throws RemoteException {
        new Emails().persist(email);
        return("Enviado com sucesso!!");
    }

    @Override
    public void salvar(Pessoa pessoa) throws RemoteException {
        new Pessoas().persist(pessoa);
    }

    @Override
    public List<Pessoa> listaPessoas() throws RemoteException {
        return new Pessoas().findAll();
    }

    @Override
    public Long latencia(byte[] array) throws RemoteException {
        Long time1 = System.currentTimeMillis();
        
        System.out.println(array.toString());
        try {
            Thread.sleep(50);
        } catch (InterruptedException ex) {}
        
        Long time2 = System.currentTimeMillis();
        
        return time2 - time1;
    }

}
