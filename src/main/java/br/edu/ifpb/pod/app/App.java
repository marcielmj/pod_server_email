package br.edu.ifpb.pod.app;

import br.edu.ifpb.pod.rmi.EmailServer;
import br.edu.ifpb.pod.task.Schedule;
import java.rmi.AlreadyBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.Timer;

/**
 *
 * @author marciel
 */
public class App {

    public static void main(String[] args) throws AlreadyBoundException {
        Registry registry;
        Timer timer = new Timer();

        try {
            registry = LocateRegistry.createRegistry(9999);
            registry.bind("Fachada", new EmailServer());
        } catch (RemoteException ex) {
            System.out.println("Oops, ocorreu um erro ao iniciar o servidor...");
        }
        
        timer.schedule(new Schedule(), 0, 300 * 60);
    }

}
