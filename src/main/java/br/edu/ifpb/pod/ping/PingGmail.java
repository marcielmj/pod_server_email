package br.edu.ifpb.pod.ping;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 *
 * @author marciel
 */
public class PingGmail {

    public static boolean ping() {
        boolean result = false;

        try {
            URL url = new URL("http://smtp.gmail.com");
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.connect();
            result = true;
        } catch (MalformedURLException ex) {
            System.out.println("Não existe conexão com a Internet...");
            result = false;
        } catch (IOException ex) {
            System.out.println("Não existe conexão com a Internet...");
            result = false;
        }

        return result;
    }
}
